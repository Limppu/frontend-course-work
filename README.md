-SDS Front-End Coursework

-

-The tutorials and project for the SDS Front-End course

-

-Getting Started

-

-These instructions will inform you how to access my project site and will explain the different folders.

-

-Prerequisites

-

-What things you need to install the software and how to install them

-

-A browser to access the site at : https://www.youtube.com/watch?v=QgTtcAu-y-M&feature=youtu.be

-Visual Studio Code or similar tool to access the code if so wanted.

-

-Installing

-

-Install the mentioned programs from their respected sites to get the latest versions.

-

-https://code.visualstudio.com/

-

-Accessing the folders

-

-Course project is in the course_project folder.

-

-Modules folder contains all the work i did when following the tutorials. introduction and modern_portfolio are leftovers from my initial trials which ended up too messy. The modern_portfolio-master contains the tutorial files from the tutorial makers github.

-

-The project site can be accessed from previously mentioned link or by downloading the files and accessing them locally

-

-The web site contains a raw portfolio with a developed menu / navigation system.

-

-Sub pages of the site hold no valid information and are gibberish and focus on presenting a few different display possibilities for a portfolio site.

-

-Built With

-

-    Visual Studio Code - The code editor used

-

-Author

-

-    Tuomas Liimatta